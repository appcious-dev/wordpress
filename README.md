# Word-Press Landing Pages

Our current external facing landing pages are implemented in WordPress. This allows content modification by non-developers.

## Architecture:

WordPress keeps content in two locations: backend database and the local file system. To make sure content changes are properly tested and versioned, we maintain a separate QA environment where changes are initially implemented. Once all changes have been verified, the devop engineer will extract the latest content from the QA environment and push them into the source code repository. The updated content is then packaged up in a container and deployed to the production environment.

### QA Environment

The QA environment uses the `wordpress_qa` database stored in the globally replicated MariaDB. The local file system for QA is stored in the globally shared `us-west-2_qa` GlusterFS volume. Since both database and file servers are globally shared, changes made by one QA instance will be immediately reflected on all QA servers.

To prevent `w3server` container from overwriting content in the QA environment, the Ansible deployment scripts set the `$W3_OVERWRITE` environment variable to `no` when deploying to QA. This tells `w3server` to preserve any existing content found in the system.

### Extracting from QA

To extract the latest WordPress content from QA:

    # Attach to any db_global container:
    $ docker exec -it db_global /bin/bash

    # Create MySQL dump:
    $ mysqldump -u wordpress_qa -ppassword wordpress_qa > /etc/ssl/mysql/database.sql
    # This will create database.sql file under /data/xvdf/db_global/ssl in the host.

    # Log into the QA system in us-west-2a, and tar up WordPress directory:
    $ tar czf /tmp/wp.tgz /data/glusterfs/us-west-2_qa/wp

    # Copy out both database.sql and wp.tgz file, and push them into repo.

## Production Environment

The production environment uses the `wordpress_prod` database stored in the globally replicated MariaDB servers. Content files are kept in the local volume, and are locked down to prevent modification.

### Deploy to Production

To deploy latest WordPress content to production:

    # Copy over database.sql to any db_global container, and replace dev.perscale.com with www.perscale.com
    $ sed -i 's|dev.perscale.com|www.perscale.com|g' database.sql

    # Apply database changes to database
    $ mysql -u wordpress_prod -ppassword wordpress_prod < /etc/ssl/mysql/database.sql

    # Deploy w3server containers to prod
