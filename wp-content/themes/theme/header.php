<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.png" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.png" rel="apple-touch-icon-precomposed">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    		<meta name="keywords" content="Perscale, apps in the browser, desktop apps in the cloud, business apps. productivity apps, business apps, office apps, apps on any device, chromebook apps, iPad, Photoshop, Microsoft Office, Adobe">
		<meta name="description" content="Run your software in a browser on any device including iPad and Chromebook. Integrated with Dropbox, Google Drive, OneDrive. Sign up now!">

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<style type="text/css">
        .nav ul {
          list-style: none !important;
          float: right !important;
        }
        .nav ul li{
	      display: inline !important;
        }
        .header_msgs{
 		display: none;
		}
        </style>
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
jQuery(document).ready(function(){
jQuery("#menufication-top").append("<div class='logoSmall showit'><a href='https://www.perscale.com'><img src='https://www.perscale.com/wp-content/themes/theme/img/logo.png' alt='Logo' class='logo-img'></a></div>");

  if($(document).width() <= 768) {


  }
else{
       jQuery(".logoSmall").removeClass("noshowitLogo").addClass("showit");
}

});
        </script>
<!-- Facebook Pixel Code -->
<script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function()
{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}
;if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', 'FACEBOOK_TRACK_UID');</script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style_new.css">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
<div class="toptop" data-0="top:0px;" data-100="top:-20px;">
	<div class="social-icons-header">
<div class="social-icons-container">
<div class="blank-line">
<a href="https://www.linkedin.com/company/perscale" target="_blank"><i class="fa fa-linkedin"></i></a>
</div>
<div class="gray-line">
<a href="https://twitter.com/perscale" target="_blank"><i class="fa fa-twitter"></i></a>
</div>
	<div class="gray-line">
<a href="https://www.facebook.com/perscale" target="_blank"><i class="fa fa-facebook"></i></a>
</div>
<div class="clearit"></div>
</div>
	</div>

<style type="text/css">
@media only screen and (max-width: 792px){
.new_cont{
display:none
}.logo{
/* change this if you don't wont to see logo on 792px screen
  display:none
*/
}
@media only screen and (max-width: 777px){
/*
 .logo{
   display:none
 }
}
#menufication-top{
    background: url(https://www.perscale.com/wp-content/uploads/2015/09/Perscale-Logo.png) no-repeat center center !important;
    background-size: 104px 21px !important;
    background-color: #FFF !important;
}
*/
}
</style>
			<header class="header clear" role="banner">

					<!-- logo -->
					<div class="container">
					<div class="logo showit">
						<a href="<?php echo home_url(); ?>">
							<script>
							document.write("<img src='" + wp_menufication['headerLogo'] + "' alt='Logo' class='logo-img'>")
							</script>
						</a>
					</div>
					<!-- /logo -->

					<!-- nav -->
					<nav class="nav" role="navigation" style="margin-top: -13px;padding-bottom: 13px;">
						<?php theme_nav(); ?>
					</nav>
					<p></p>
					<!-- /nav -->
</div>
			</header>
			<!-- /header -->
</div>
<div style="margin-top:74px;" class="showit">&nbsp;</div>