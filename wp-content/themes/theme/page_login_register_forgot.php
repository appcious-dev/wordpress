<?php /* Template Name: Login Register Forgot Password Template */ get_header(); ?>

<!--
<div style="background:#000;">
<div class="jumbotron login" data-0="opacity:1;" data-100="opacity:.1;">
 -->
<div>
<div class="jumbotron login">
      <div class="container about_us">
      </div>
    </div>
</div>

<div class="jumbotron about_us-title">
      <div class="container">
<h1><?php echo get_the_title();?></h1>
<p><?php echo get_the_excerpt();?></p>
</div></div>

<div class="jumbotron about_us-content">
      <div class="container">
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php
        // check for hash
        if( get_the_ID() == "26" ){
          $uri = $_SERVER['REQUEST_URI'];
          $explodeBySlash = explode("/", $uri);
          $hash = $explodeBySlash[count($explodeBySlash)-1];
          if( trim($hash) == ""){
            header("Location:".get_home_url());
          }
        }
        the_content(); ?>


				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php endif; ?>




      </div>
    </div>






<!-- <div class="container">
     <div class="row">
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
       </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        </div>
      </div>

      <hr>


    </div> -->


<?php //get_sidebar(); ?>
<!-- <div class="jumbotron intrigued" data-bottom-top="opacity:0;" data-bottom="opacity:1;">
<div class="container">
<div class="left">
<h1>Intrigued?</h1>
Access your applications where ever you go with Perscale.
</div>
<div class="right">
<a class="btn btn-primary btn-transparent btn-home-page-green button--isi" href="#">Start Your Free Trial Now</a>
</div>
<div class="clearit"></div>
</div>
</div>
 -->

<?php get_footer(); ?>
