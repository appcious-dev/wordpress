<?php /* Template Name: FAQ Template */ get_header(); ?>

<!--
<div style="background:#000;">
<div class="jumbotron faq" data-0="opacity:1;" data-100="opacity:.1;">
 -->
<div>
<div class="header_msgs jumbotron faq" >
  <div class='messagesDiv'>
    <div class='gray_info_part_header'>
      <p class='error headerInfo topError'></p>
      <p class='warning headerInfo topWarning'></p>
      <p class='warning headerInfo topSuccess'></p>
      <input class="btn btn-primary btn-transparent btn-home-page" onClick='hideHeaderMessage()' type="button" value="Ok" />
    </div>
  </div>
</div>
<div class="jumbotron faq">
      <div class="container faq">
      <!-- article -->
      <article id="post-5" class="post-5 page type-page status-publish has-post-thumbnail hentry">
<h2>Ready to get your<br>
processing power on demand?
</h2>
<br class="clear">

<div class="frmHeaderDiv">
  <form id="frmFaqApply" action="#" method="get">
    <input name="email" type="email" id="email___" placeholder="Enter your email" />
    <input class="btn btn-primary btn-transparent btn-home-page" type="submit" value="Start Free Trial Now" />
  </form>
</div>
<br class="clear">
<!-- <a class="btn btn-primary btn-transparent btn-faq" href="<?php echo get_permalink(26);?>">Start Free Trial Now.</a> -->
      </article>
      <!-- /article -->
      </div>
    </div>
</div>

<div class="jumbotron faq-title">
      <div class="container">
<h1>Frequently Asked Questions</h1>
<p>Perscale is an application portal and app streaming service launched by Perscale.
Perscale enables you to select applications, and start using them without downloading or installing anything.</p>
</div></div>

<div class="jumbotron faq-content">
      <div class="container">
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
				<br class="clear">
				<?php edit_post_link(); ?>
			</article>
			<!-- /article -->
		<?php endwhile; ?>
		<?php endif; ?>
      </div>
    </div>

<!-- <div class="container">
     <div class="row">
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
       </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        </div>
      </div>

      <hr>


    </div> -->

<script type="text/javascript">
// <![CDATA[

function resendLink(email){
		$.ajax({contentType: "application/json; charset=utf-8",
			"method" : "POST",
			"url" : "/forgot_password/initialize",
			"data" : JSON.stringify({'email':email}),
		    'dataType': 'json',	
		});
		hideHeaderMessage();
		hideFooterMessages();
return false;
	}

 function hideFooterMessages(){
    jQuery(".footer_msgs").css("display",'none');
    $("#email__").val('');

    $("#resendLink").remove();
  }
  function hideHeaderMessage(){
    jQuery(".header_msgs").css("display",'none');
    $("#resendLink").remove();
$("#email___").val('');
$("input").prop("disabled","");
  }


jQuery(document).ready(function(){

 jQuery("#frmFooterApply").submit(function (e) {
    e.preventDefault();
	$(document).keyup(function (e) {
		if (e.keyCode == 13) {
			if(jQuery(".gray_info_part").css("display")=="block"){
				hideFooterMessages();
				return false;
			}
		}
	});
	if(jQuery(".footer_msgs").css("display")=="block"){
		return false;
	}
    jQuery(".footerInfo.topError").html("");
    jQuery(".footerInfo.topWarning").html("");
    jQuery(".footerInfo.topSuccess").html("");
    jQuery("#frmFooterApply").find("input[type*='submit']").attr("disabled","disabled");
    
    

jQuery(".footer_msgs").height(jQuery(".footer_msgs.jumbotron").next().height());
    var marginTop = (jQuery(".jumbotron.footer_msgs").last().height()/2)-(jQuery(".gray_info_part").height()/2)-20;
    jQuery(".gray_info_part").css("margin-top",parseInt(marginTop-30));

    if( jQuery("#frmFooterApply").find("input[name='email']").val().trim() == ""){
         jQuery(".footerInfo.topError").first().html("Please enter your email address.");
         jQuery(".footerInfo.topError").first().addClass("showIt");
         jQuery("#frmFooterApply").find("input[type*='submit']").prop("disabled", false);
         jQuery(".footer_msgs").css("display",'block');
    }
    else{
        sbmtHomePageFooterRegistration( jQuery("#frmFooterApply") );
    }
  });
  jQuery("#frmFaqApply").submit(function (e) {
    e.preventDefault();
	$(document).keyup(function (e) {
		if (e.keyCode == 13) {
			if(jQuery(".gray_info_part").css("display")=="block"){
				hideFooterMessages();
				return false;
			}
		}
	});
	if(jQuery(".header_msgs").css("display")=="block"){
		return false;
	}
    jQuery(".footerInfo.topError").html("");
    jQuery(".footerInfo.topWarning").html("");
    jQuery(".topSuccess").html("");
    jQuery(".header_msgs").height(jQuery(".jumbotron.faq").last().height());
    var marginTop = (jQuery(".jumbotron.faq").last().height()/2)-(jQuery(".gray_info_part_header").height()/2)-20;
    jQuery(".gray_info_part_header").css("margin-top",parseInt(marginTop));
    jQuery("#frmFaqApply").find("input[type*='submit']").attr("disabled","disabled");
    if( jQuery("#frmFaqApply").find("input[name='email']").val().trim() == ""){
         jQuery(".topError").first().html("Please enter your email address.");
         jQuery(".topError").first().addClass("showIt");
         jQuery("#frmFaqApply").find("input[type*='submit']").prop("disabled", false);
         jQuery(".header_msgs").css("display",'block');
    }
    else{
        sbmtFaqApply( jQuery("#frmFaqApply") );
    }
  });
  function sbmtHomePageFooterRegistration(formObj) {

      var params = {};
      params.email = formObj.find("input[name='email']").val();
      params.type = 'registration';
      jQuery(".footerInfo.showIt").removeClass("showIt");
      jQuery.ajax({
          url: "https://www.perscale.com/beta.html",
          method: "POST",
          data: JSON.stringify(params),
          'processData': false,
          'contentType': 'application/json'
      })
      .done(function(response) {
          if (response.status == "error" || response.status == 'warning') {
              
		              jQuery(".footer_msgs").css("display",'block');
		              jQuery(".footerInfo.topError").html(response.message);
		              jQuery(".footerInfo.topError").addClass("showIt");
		              formObj.find("input[type*='submit']").prop("disabled", false);
				if(response.status=="warning"){
					$(".gray_info_part").css("width","340px");
					$(".btn-home-page[value='Ok']").css({
					"width":"150px"
					}).addClass("newStyle");
					$(".gray_info_part").css("background","#edebea").append('<a href="javascript:void(0);" id="resendLink" style="font-size:12px;color:#333;" onclick="resendLink(\''+params.email+'\')"><br />Resend Registration Email</a>');
					$(".footerInfo.error").css("font-weight","normal !important");
			
				}
          } else if (response.status == "login") {
              window.location.href =  "https://"+document.domain + "/home";
          } else {
              jQuery(".footer_msgs").css("display",'block');
              jQuery(".footerInfo.topSuccess").html(response.message);
              jQuery(".footerInfo.topSuccess").addClass("showIt");
              formObj.find("input[type*='submit']").prop("disabled", false);
              $("body").append('<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6029911129427&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>');
              window._fbq = window._fbq || [];
              window._fbq.push(['track', '6029911129427', {'value':'0.00','currency':'USD'}]);
              console.log("success");
          }
      });
  }
  function sbmtFaqApply(formObj) {
$(document).keyup(function (e) {
if (e.keyCode == 13) {
if(jQuery(".header_msgs").css("display")=="block"){
hideHeaderMessage();
return false;
}

}
});
if(jQuery(".header_msgs").css("display")=="block"){
return false;
}

      var params = {};
      params.email = formObj.find("input[name='email']").val();
      params.type = 'registration';
      jQuery(".showIt").removeClass("showIt");
      jQuery.ajax({
          url: "https://www.perscale.com/beta.html",
          method: "POST",
          data: JSON.stringify(params),
          'processData': false,
          'contentType': 'application/json'
      })
      .done(function(response) {
          if (response.status == "error" || response.status == 'warning') {
              jQuery(".header_msgs").css("display",'block');
              jQuery(".topError").html(response.message);
              jQuery(".topError").addClass("showIt");
              formObj.find("input[type*='submit']").prop("disabled", false);
if(response.status=="warning"){
$(".gray_info_part_header").css("width","340px");
$(".btn-home-page[value='Ok']").css({
"left":"0",
"margin-left":"0",
"width":"auto"
}).addClass("newStyle");
$(".gray_info_part_header").css("background","#edebea").append('<a href="#" id="resendLink" style="color:#333;" onclick="resendLink(\''+params.email+'\')"><br />Resend Registration Email</a>');
$(".headerInfo.error").css("font-weight","normal !important");

}

          } else if (response.status == "login") {
              window.location.href =  "https://"+document.domain + "/home";
          } else {
              jQuery(".header_msgs").css("display",'block');
              jQuery(".topSuccess").html(response.message);
              jQuery(".topSuccess").addClass("showIt");
              formObj.find("input[type*='submit']").prop("disabled", false);
              $("body").append('<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6029911129427&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>');
              window._fbq = window._fbq || [];
              window._fbq.push(['track', '6029911129427', {'value':'0.00','currency':'USD'}]);
              console.log("success");
          }
      });
  }
});
// ]]>
</script>
<style>

.newStyle{
    position: initial !important;
    left: 0 !important;
    margin-left: 0 !important;
    width: auto !important;
    margin: 0 auto !important;
}
.headerInfo.error{
    border-radius: 10px;
    /*background-color: #f2dede;
    border-color: #ebccd1;*/
    color: #a94442;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: red;
    font-size : 14px !important;
    display:none;
}
.headerInfo.warning{
    border-radius: 10px;
    /*background-color: #fcf8e3;
    border-color: #faebcc; */
    color: #8a6d3b;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #F3AC1E;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.headerInfo.topSuccess{
    border-radius: 10px;
    /*border-color: #d6e9c6;
    background-color: #dff0d8;*/
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #3c763d;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.headerInfo.showIt{
    display:block !important;
}
.header_msgs{
    display: none;
    background: rgba(0,0,0,0.5) !important;
    width: 100%;
    min-height: 161px;
    background-color: rgba(0,0,0,0.5) !important;
    position: absolute;
    text-align: center;
}
.header_msgs input[type='button']{
    display: block;
    position: relative;
    left: 50%;
    margin-left: -75px;
    width: 150px;
    padding: 5px 20px;
    background: rgba(255,255,255,0.8);
    color: grey;
}
  #frmFaqApply input[type="email"] {
    background-color: rgba(168,160,151,.5);
    color: #000;
    font-weight: normal;
    border: 1px solid #fff;
    padding: 5px 10px;
    margin-left: 10px;
    font-size: 14px;
    float: left;
    margin-bottom: 10px;
}
  #frmFaqApply input[type='email']:focus{
      background-color: white;
  }
  #frmFaqApply{
    width: 200px;
  }
#frmFooterApply input[type='email'] {
    background-color: rgba(168,160,151,0.5);
    color: black;
    font-size: 14px;
    font-weight: normal;
    border: 1px solid #fff;
    padding: 5px 10px;
    margin-left: 10px;
    float: left;
    margin-bottom: 10px;
}
  #frmFooterApply input[type='email']:focus{
      background-color: white;
  }
  ::-webkit-input-placeholder {
      color:white;
  }
  ::-moz-placeholder {
      color:white;
  }
.footerInfo.error{
    border-radius: 10px;
    /*background-color: #f2dede;
    border-color: #ebccd1;*/
    color: #a94442;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: red;
    font-size : 14px !important;
    display:none;
}
.footerInfo.warning{
    border-radius: 10px;
    /*background-color: #fcf8e3;
    border-color: #faebcc; */
    color: #8a6d3b;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #F3AC1E;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.footerInfo.topSuccess{
    border-radius: 10px;
    /*border-color: #d6e9c6;
    background-color: #dff0d8;*/
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #3c763d;
    font-size : 14px !important;
    display:none;
}
.footerInfo.showIt{
    display:block !important;
}
.error{
    border-radius: 10px;
    /*background-color: #f2dede;
    border-color: #ebccd1;*/
    color: #a94442;
    padding: 10px;
    top: 27px;
    /*position: absolute;*/
    text-align: center;
    color: red;
    font-size : 14px !important;
    display:none;
    margin: auto;
}
.warning{
    margin: auto;
    border-radius: 10px;
    /*background-color: #fcf8e3;
    border-color: #faebcc;*/
    color: #8a6d3b;
    padding: 10px;
    top: 27px;
    /*position: absolute;*/
    text-align: center;
    color: #F3AC1E;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.topSuccess{
    border-radius: 10px;
    /*border-color: #d6e9c6;
    background-color: #dff0d8;*/
    padding: 10px;
    top: 27px;
    /*position: absolute;*/
    text-align: center;
    color: #3c763d;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
    margin: auto;
}
.showIt{
    display:block !important;
}
.container .left .messagesDiv{
  display: block;
  height: 20px;
  width: 630px;
  float: left;
  margin-top: 0px;
  position: absolute;
  font-weight: 200;
  font-style: 16px;
}
.footer_msgs{
    display: none;
    background: rgba(0,0,0,0.5) !important;
    width: 100%;
    min-height: 161px;
    background-color: rgba(0,0,0,0.5) !important;
    position: absolute;
    text-align: center;
}
.footer_msgs input[type='button']{
    display: block;
    position: relative;
    left: 50%;
    margin-left: -75px;
    width: 150px;
    padding: 5px 20px;
    background: rgba(255,255,255,0.8);
    color: grey;
}
.gray_info_part{
    background: #ccc;
    width: 280px;
    margin:auto;
    min-height: 90px;
    padding: 0 10px 15px 10px;
    border-radius: 5px;
}
.gray_info_part_header{
    background: rgb(237, 235, 234);
    width: 280px;
    margin:auto;
    min-height: 90px;
    padding: 0 10px 15px 10px;
    border-radius: 5px;
}
</style>


<?php //get_sidebar(); ?>
<!-- <div class="jumbotron intrigued" data-bottom-top="opacity:0;" data-bottom="opacity:1;"> -->
<div class="footer_msgs jumbotron intrigued" >
  <div class='messagesDiv'>
    <div class='gray_info_part'>
      <p class='error footerInfo topError'></p>
      <p class='warning footerInfo topWarning'></p>
      <p class='warning footerInfo topSuccess'></p>
      <input class="btn btn-primary btn-transparent btn-home-page" onClick='hideFooterMessages()' type="button" value="Ok" />
    </div>
  </div>
</div>
<div class="jumbotron intrigued">
<div class="container">
<div class="left">
<h1>Intrigued?</h1>
Access your applications where ever you go with Perscale.

</div>
<div class="right">

<div class="frmFooterDiv">
  <form id="frmFooterApply" action="#" method="get">
    <input name="email" type="email" id="email__" placeholder="Enter your email" />
    <input class="btn btn-primary btn-transparent btn-home-page" type="submit" value="Start Free Trial Now" />
  </form>
</div>
<!-- <a class="btn btn-primary btn-transparent btn-home-page-green" href="<?php echo get_permalink(26);?>">Start Your Free Trial Now</a> -->

</div>
<div class="clearit"></div>
</div>
</div>

<?php get_footer(); ?>