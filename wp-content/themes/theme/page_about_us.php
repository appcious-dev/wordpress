<?php /* Template Name: About Us Template */ get_header(); ?>

<!--
<div style="background:#000;">
<div class="jumbotron about_us" data-0="opacity:1;" data-100="opacity:.1;">
 -->
<div>
<div class="jumbotron about_us">
      <div class="container about_us">
      </div>
    </div>
</div>

<div class="jumbotron about_us-title">
      <div class="container">
<h1>LEARN ABOUT OUR TEAM AND CULTURE</h1>
<p>We are a startup dedicated to simplifying the development, delivery and use of software.
Our vision is to enable consumers to use the software they love and need from any device wherever they are.  </p>
</div></div>
<!-- Facebook Conversion Code for Leads - Perscale 1 -->

<div class="jumbotron about_us-content">
      <div class="container">
<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
				<br class="clear">
				<?php edit_post_link(); ?>
			</article>
			<!-- /article -->
		<?php endwhile; ?>
		<?php endif; ?>
      </div>
    </div>

<!-- <div class="container">
     <div class="row">
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
       </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        </div>
      </div>
      <hr>
    </div> -->


<script type="text/javascript">
// <![CDATA[
	
	function resendLink(email){
		$.ajax({contentType: "application/json; charset=utf-8",
			"method" : "POST",
			"url" : "/forgot_password/initialize",
			"data" : JSON.stringify({'email':email}),
		    'dataType': 'json',	
		});
		hideHeaderMessage();
		hideFooterMessages();
return false;
	}

 function hideFooterMessages(){
    jQuery(".footer_msgs").css("display",'none');
    $("#email__").val('');
    $("#resendLink").remove();
$(".gray_info_part").css("margin-top","");
  }
  function hideHeaderMessage(){
    jQuery(".header_msgs").css("display",'none');
    $("#resendLink").remove();
  }
jQuery(document).ready(function(){
  // var widthOfDocument = $(this).width();
  // var dropbox = jQuery("#dropbox");
  // var oneDrive = jQuery("#one-drive");
  // var googleDrive = jQuery("#google-drive");
  // if(widthOfDocument >= 1200){

  // }
  // else if(widthOfDocument < 1200 && widthOfDocument > 900){

  // }
  // else if(widthOfDocument <= 900){

  // }
  // else{

  // }
  jQuery("#frmFooterApply").submit(function (e) {
    e.preventDefault();
	$(document).keyup(function (e) {
		if (e.keyCode == 13) {
			if(jQuery(".gray_info_part").css("display")=="block"){
				hideFooterMessages();
				return false;
			}
		}
	});
	if(jQuery(".footer_msgs").css("display")=="block"){
		return false;
	}
    jQuery(".footerInfo.topError").html("");
    jQuery(".footerInfo.topWarning").html("");
    jQuery(".footerInfo.topSuccess").html("");
    jQuery("#frmFooterApply").find("input[type*='submit']").attr("disabled","disabled");
    
    

jQuery(".footer_msgs").height(jQuery(".footer_msgs.jumbotron").next().height());
    var marginTop = (jQuery(".jumbotron.footer_msgs").last().height()/2)-(jQuery(".gray_info_part").height()/2)-20;
    jQuery(".gray_info_part").css("margin-top",parseInt(marginTop));

    if( jQuery("#frmFooterApply").find("input[name='email']").val().trim() == ""){
         jQuery(".footerInfo.topError").first().html("Please enter your email address.");
         jQuery(".footerInfo.topError").first().addClass("showIt");
         jQuery("#frmFooterApply").find("input[type*='submit']").prop("disabled", false);
         jQuery(".footer_msgs").css("display",'block');
    }
    else{
        sbmtHomePageFooterRegistration( jQuery("#frmFooterApply") );
    }
  });
  function sbmtHomePageFooterRegistration(formObj) {
      var params = {};
      params.email = formObj.find("input[name='email']").val();
      params.type = 'registration';
      jQuery(".footerInfo.showIt").removeClass("showIt");
      jQuery.ajax({
          url: "https://www.perscale.com/beta.html",
          method: "POST",
          data: JSON.stringify(params),
          'processData': false,
          'contentType': 'application/json'
      })
      .done(function(response) {
          if (response.status == "error" || response.status == 'warning') {
              
		              jQuery(".footer_msgs").css("display",'block');
		              jQuery(".footerInfo.topError").html(response.message);
		              jQuery(".footerInfo.topError").addClass("showIt");
		              formObj.find("input[type*='submit']").prop("disabled", false);
				if(response.status=="warning"){
					$(".gray_info_part").css("width","340px").css("margin-top","-3px");
					$(".btn-home-page[value='Ok']").css({
					"left":"0",
					"margin-left":"0",
					"width":"auto"
					}).addClass("newStyle");
					$(".gray_info_part").css("background","#edebea").append('<a href="javascript:void(0);" id="resendLink" style="font-size:12px;color:#333;" onclick="resendLink(\''+params.email+'\')"><br />Resend Registration Email</a>');
					$(".footerInfo.error").css("font-weight","normal !important");
			
				}
          } else if (response.status == "login") {
              window.location.href =  "https://"+document.domain + "/home";
          } else {
              jQuery(".footer_msgs").css("display",'block');
              jQuery(".footerInfo.topSuccess").html(response.message);
              jQuery(".footerInfo.topSuccess").addClass("showIt");
              formObj.find("input[type*='submit']").prop("disabled", false);
              $("body").append('<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6029911129427&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>');
              window._fbq = window._fbq || [];
              window._fbq.push(['track', '6029911129427', {'value':'0.00','currency':'USD'}]);
              console.log("success");
          }
      });
  }
});
// ]]>
</script>
<style>

.my-icon {
    vertical-align: middle;
    font-size: 24px;
}
.my-icon1 {
    vertical-align: middle;
    font-size: 30px;
}

#frmFooterApply input[type='email'] {
    background-color: rgba(168,160,151,0.5);
    color: black;
    font-size: 14px;
    font-weight: normal;
    border: 1px solid #fff;
    padding: 5px 10px;
    margin-left: 10px;
    float: left;
    margin-bottom: 10px;
}
  .container .left .messagesDiv{
    display: block;
    height: 20px;
    margin-top: 12px;
    float: left;
    position: absolute;
    margin-left: 0px;
    font-size: 16px;
    font-weight: 200;
    width: 630px;
}
  #frmFooterApply input[type='email']:focus{
      background-color: white;
  }
  ::-webkit-input-placeholder {
      color:white;
  }
  ::-moz-placeholder {
      color:white;
  }
.footerInfo.error{
    border-radius: 10px;
 /*   background-color: #f2dede;
    border-color: #ebccd1;*/
    color: #a94442;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: red;
    font-size : 14px !important;
    display:none;
}
.footerInfo.warning{
    border-radius: 10px;
  /*  background-color: #fcf8e3;
    border-color: #faebcc;*/
    padding: 5px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #F3AC1E;
    font-size : 13px !important;
    font-weight: bold !important;
    display:none;
}
.footerInfo.topSuccess{
    border-radius: 10px;
  /*  border-color: #d6e9c6;
    background-color: #dff0d8;*/
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #3c763d;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.footerInfo.showIt{
    display:block !important;
}
.footer_msgs{
    display: none;
    background: rgba(0,0,0,0.5) !important;
    width: 100%;
    min-height: 161px;
    background-color: rgba(0,0,0,0.5) !important;
    position: absolute;
    text-align: center;
}
.footer_msgs input[type='button']{
    display: block;
    position: relative;
    left: 50%;
    margin-left: -75px;
    width: 150px;
    padding: 5px 20px;
    background: rgba(255,255,255,0.8);
    color: grey;
}
.gray_info_part{
    background: #ccc;
    width: 280px;
    margin:auto;
    min-height: 90px;
    padding: 0 10px 15px 10px;
    border-radius: 5px;
}
.gray_info_part_header{
    background: #ccc;
    width: 280px;
    margin:auto;
    min-height: 90px;
    padding: 0 10px 15px 10px;
    border-radius: 5px;
}

.headerInfo.error{
    border-radius: 10px;
    /*background-color: #f2dede;
    border-color: #ebccd1;*/
    color: #a94442;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: red;
    font-size : 14px !important;
    display:none;
}
.headerInfo.warning{
    border-radius: 10px;
    /*background-color: #fcf8e3;
    border-color: #faebcc; */
    color: #8a6d3b;
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #F3AC1E;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.headerInfo.topSuccess{
    border-radius: 10px;
    /*border-color: #d6e9c6;
    background-color: #dff0d8;*/
    padding: 10px;
    top: 5px;
    position: relative;
    text-align: center;
    color: #3c763d;
    font-size : 14px !important;
    font-weight: bold !important;
    display:none;
}
.headerInfo.showIt{
    display:block !important;
}
.header_msgs{
    display: none;
    background: rgba(0,0,0,0.5) !important;
    width: 100%;
    min-height: 161px;
    background-color: rgba(0,0,0,0.5) !important;
    position: absolute;
    text-align: center;
    z-index: 1;
}

.newStyle{
    position: initial !important;
    left: 0 !important;
    margin-left: 0 !important;
    width: auto !important;
    margin: 0 auto !important;
}
.header_msgs input[type='button']{
    display: block;
    position: relative;
    left: 50%;
    margin-left: -75px;
    width: 150px;
    padding: 5px 20px;
    background: rgba(255,255,255,0.8);
    color: grey;
}
</style>

<?php //get_sidebar(); ?>
<!-- <div class="jumbotron intrigued" data-bottom-top="opacity:0;" data-bottom="opacity:1;"> -->
<div class="footer_msgs jumbotron intrigued" >
  <div class='messagesDiv'>
    <div class='gray_info_part'>
      <p class='error footerInfo topError'></p>
      <p class='warning footerInfo topWarning'></p>
      <p class='warning footerInfo topSuccess'></p>
      <input class="btn btn-primary btn-transparent btn-home-page" onClick='hideFooterMessages()' type="button" value="Ok" />
    </div>
  </div>
</div>
<div class="jumbotron intrigued">
<div class="container">
<div class="left">
<h1>Intrigued?</h1>
Access your applications where ever you go with Perscale.
</div>
<div class="right">
  <div class='frmFooterDiv'>
    <form id="frmFooterApply" action="#" method="get">
    <input name="email" type="email" id="email__" placeholder="Enter your email" />
    <input class="btn btn-primary btn-transparent btn-home-page" type="submit" value="Start Free Trial Now" />
    </form>
  </div>
<!-- <a class="btn btn-primary btn-transparent btn-home-page-green" href="<?php echo get_permalink(26);?>">Start Your Free Trial Now</a> -->

</div>
<div class="clearit"></div>
</div>
</div>

<?php get_footer(); ?>