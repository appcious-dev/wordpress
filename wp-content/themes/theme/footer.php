			<!-- footer -->
			<footer class="footer" role="contentinfo">

<div class="container">
<div class="left">
				<!-- copyright -->
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> Copyright - <?php bloginfo('name'); ?>.
				</p>

</div>
<div class="right">
<!-- <a href="<?php echo get_permalink(64);?>">About Us</a>
   |
   <a href="<?php echo get_permalink(20);?>">FAQ</a>
   |
   <a href="<?php echo get_permalink(26);?>">Sign Up</a>
   |
   <a href="<?php echo get_permalink(23);?>">Login</a>
   |
   <a href="<?php echo get_permalink(26);?>">Registration</a>
   |    -->
   <a href="<?php echo get_permalink(32);?>">Privacy Policy</a>
   <!--
   | <a href="<?php echo get_permalink(96);?>">Acceptable Use Policy</a> -->
   |
   <a href="<?php echo get_permalink(29);?>">Terms of Service</a>

</div>

				<!-- /copyright -->
</div>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-62361013-2', 'auto');
		ga('send', 'pageview');
		</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>



<script>
/* Thanks to CSS Tricks for pointing out this bit of jQuery
http://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;
   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}
$(window).load(function() {
  equalheight('.col-md-4 .inside');
});
$(window).resize(function(){
  equalheight('.col-md-4 .inside');
});
fbq('track', 'ViewContent');
</script>

<script type="text/javascript" src="/wp-content/themes/theme/js/skrollr.min.js"></script>

  <script type="text/javascript">

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
 // some code..
}
else{

    var s = skrollr.init({
      forceHeight: true
    });

    }


  </script>


	</body>
</html>
